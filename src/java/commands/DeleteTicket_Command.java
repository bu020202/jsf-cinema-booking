package commands;

import database.active_record.TicketHandler;
import managed_Beans.BackingBean;
import managed_Beans.CustomerBB;

public class DeleteTicket_Command implements Command{
    
    private final CustomerBB backingBean;
    private final TicketHandler ticketHandler;
    
    public DeleteTicket_Command(BackingBean bb) {
        backingBean = (CustomerBB)bb;
        ticketHandler = new TicketHandler();
    }

    @Override
    public String execute() {
        String outcome = "";
        
        if(ticketHandler.deleteTicket(backingBean.getTicketToDelete())) {
            backingBean.setTicketToDelete(null);
            outcome = "myTickets.xhtml";
        }
        
        return outcome;
    }
}
