package commands;

import beans.Film;
import database.active_record.FilmHandler;
import java.util.ArrayList;
import managed_Beans.*;

public class GetAllFilms_Command implements Command{
    
    private final BackingBean backingBean;
    private final FilmHandler filmHandler;
    
    public GetAllFilms_Command(BackingBean bb) {
        filmHandler = new FilmHandler();
        backingBean = bb;
    }
    
    @Override
    public String execute() {
        
        String outcome;
        
        ArrayList<Film> films = filmHandler.findAllFilms();
        
        if(films == null) {
            outcome = "error.xhtml";
        }
        else {
            outcome = "adminHome.xhtml";
            backingBean.setFilms(films);
        }    
        return outcome;
    }
}