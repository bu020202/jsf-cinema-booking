package commands;

import beans.Film;
import database.active_record.FilmHandler;
import java.util.ArrayList;
import managed_Beans.*;

public class GetAllFilms_AdminCommand implements Command{
    
    private final AdministratorBB backingBean_Admin;
    private final FilmHandler filmHandler;
    
    public GetAllFilms_AdminCommand(BackingBean bb) {
        filmHandler = new FilmHandler();
        backingBean_Admin = (AdministratorBB)bb;
    }
    
    @Override
    public String execute() {
        
        String view;
        
        ArrayList<Film> films = filmHandler.findAllFilms();
        
        if(films == null) {
            view = "error.xhtml";
        }
        else {
            view = "adminHome.xhtml";
            backingBean_Admin.setFilms(films);
        }    
        return view;
    }
}