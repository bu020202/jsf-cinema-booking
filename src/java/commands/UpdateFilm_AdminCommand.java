package commands;

import database.active_record.FilmHandler;
import managed_Beans.AdministratorBB;
import managed_Beans.BackingBean;

public class UpdateFilm_AdminCommand implements Command {
    
    private final AdministratorBB backingBean_Admin;
    
    public UpdateFilm_AdminCommand(BackingBean bb) {
        backingBean_Admin = (AdministratorBB)bb;
    }
    
    @Override
    public String execute() {
        
        String view = null;
        FilmHandler filmHandler = new FilmHandler();
        if(filmHandler.updateFilm(backingBean_Admin.getFilmToUpdate())) {
            backingBean_Admin.setFilmToUpdate(null);
            view = "adminHome.xhtml";
        }
        else {
            
        }
        return view;
    }
}
