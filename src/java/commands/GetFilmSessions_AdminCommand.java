package commands;

import beans.Session;
import database.active_record.SessionHandler;
import managed_Beans.AdministratorBB;
import managed_Beans.BackingBean;
import java.util.ArrayList;

public class GetFilmSessions_AdminCommand implements Command{
    
    private final AdministratorBB backingBean_Admin;
    private final SessionHandler sessionHandler;
    
    public GetFilmSessions_AdminCommand(BackingBean bb) {
        backingBean_Admin = (AdministratorBB)bb;
        sessionHandler = new SessionHandler();
    }

    @Override
    public String execute() {
        
        String view = null;
        
        int filmId = backingBean_Admin.getFilmToDisplay().getFilmId();
        ArrayList<Session> sessions = sessionHandler.findSessionsByFilmId(filmId);
        
        if(sessions == null) {
            view = "error.xhtml";
        }
        else{
            view = "success";
            backingBean_Admin.setSessions(sessions);
        }
        return view;
    }
}