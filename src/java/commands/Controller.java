package commands;

import managed_Beans.*;

public class Controller {

    public String executeCommand(String userCommand, BackingBean managedBean) {
        Command command = CommandFactory.createCommand(userCommand, managedBean);
        return command.execute();
    }
}