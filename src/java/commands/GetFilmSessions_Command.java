package commands;

import beans.Session;
import database.active_record.SessionHandler;
import managed_Beans.AdministratorBB;
import managed_Beans.BackingBean;
import java.util.ArrayList;

public class GetFilmSessions_Command implements Command{
    
    private final BackingBean backingBean;
    private final SessionHandler sessionHandler;
    
    public GetFilmSessions_Command(BackingBean bb) {
        backingBean = bb;
        sessionHandler = new SessionHandler();
    }

    @Override
    public String execute() {
        
        String outcome = null;
        
        int filmId = backingBean.getFilmToDisplay().getFilmId();
        ArrayList<Session> sessions = sessionHandler.findSessionsByFilmId(filmId);
        
        if(sessions == null) {
            outcome = "error.xhtml";
        }
        else{
            outcome = "success";
            backingBean.setSessions(sessions);
        }
        return outcome;
    }
}