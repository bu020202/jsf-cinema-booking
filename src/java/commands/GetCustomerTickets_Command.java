package commands;

import beans.Ticket;
import database.active_record.TicketHandler;
import managed_Beans.BackingBean;
import managed_Beans.CustomerBB;
import java.util.ArrayList;


public class GetCustomerTickets_Command implements Command{
    
    private final CustomerBB backingBean;
    private final TicketHandler ticketHandler;
    
    public GetCustomerTickets_Command(BackingBean bb) {
        backingBean = (CustomerBB)bb;
        ticketHandler = new TicketHandler();
    }

    @Override
    public String execute() {
        String outcome = "";
        
        String username = backingBean.getUsername();
        ArrayList<Ticket> tickets = ticketHandler.findTicketsByUsername(username);
        
        if(tickets != null) {
            outcome = "success";
            backingBean.setTickets(tickets);
        }
        
        return outcome;
    }
}
