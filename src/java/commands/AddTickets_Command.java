package commands;

import database.active_record.TicketHandler;
import managed_Beans.BackingBean;
import managed_Beans.CustomerBB;

public class AddTickets_Command implements Command{
    
    private final CustomerBB backingBean;
    private final TicketHandler ticketHandler;
    
    public AddTickets_Command (BackingBean bb) {
        backingBean = (CustomerBB)bb;
        ticketHandler = new TicketHandler();
    }

    @Override
    public String execute() {
        
        String outcome = "";
        
        int numberOfTickets = backingBean.getNumberOfTicketsToCreate();
        
        for(int i = 0; i < numberOfTickets; i++) {
            ticketHandler.createTicket(backingBean.getTicketToCreate());
        }
        backingBean.setTicketToCreate(null);
        backingBean.setSessionToDisplay(null);
        backingBean.setNumberOfTicketsToCreate(1);
        
        outcome = "bookingConfirmation.xhtml";
        
        return outcome;
    }
    
}
