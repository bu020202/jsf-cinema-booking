package commands;

import beans.Film;
import beans.Session;
import beans.Ticket;
import database.active_record.FilmHandler;
import database.active_record.SessionHandler;
import database.active_record.TicketHandler;
import java.util.ArrayList;
import managed_Beans.AdministratorBB;
import managed_Beans.BackingBean;

public class DeleteFilm_AdminCommand implements Command{
    
    private final AdministratorBB backingBean_Admin;
    private final SessionHandler sessionHandler;
    private final TicketHandler ticketHandler;
    private final FilmHandler filmHandler;
    
    public DeleteFilm_AdminCommand(BackingBean bb) {
        backingBean_Admin = (AdministratorBB)bb;
        sessionHandler = new SessionHandler();
        ticketHandler = new TicketHandler();
        filmHandler = new FilmHandler();
    }

    @Override
    public String execute() {
        
        String view = "/error.xhtml";
        
        try {
            Film filmToDelete = backingBean_Admin.getFilmToDelete();
            ArrayList<Session> sessionsToDelete = sessionHandler.findSessionsByFilmId(filmToDelete.getFilmId());
            
            if(sessionsToDelete != null && sessionsToDelete.size() > 0) {
                for(Session session : sessionsToDelete) {
                    ArrayList<Ticket> ticketsToDelete = ticketHandler.findTicketsBySessionId(session.getSessionId());
                    
                    if(ticketsToDelete != null && ticketsToDelete.size() > 0) {
                        for(Ticket ticket : ticketsToDelete) {
                            ticketHandler.deleteTicket(ticket);
                        }
                    }
                    
                    sessionHandler.deleteSession(session);
                }
            }
            
            filmHandler.deleteFilm(filmToDelete);
            backingBean_Admin.setFilmToDelete(null);
            view = "adminHome.xhtml";
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        
        return view;
    }
}