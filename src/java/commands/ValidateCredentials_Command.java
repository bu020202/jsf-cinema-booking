package commands;

import managed_Beans.BackingBean;
import beans.Customer;
import beans.Administrator;
import database.active_record.AccountHandler;


public class ValidateCredentials_Command implements Command{

    private final BackingBean backingBean;
    private final AccountHandler accountHandler;
    
    public ValidateCredentials_Command(BackingBean bb) {
        backingBean = bb;
        accountHandler = new AccountHandler();
    }
    
    @Override
    public String execute() {
        
        String outcome = null;
        
        String username = backingBean.getUsername();
        String password = backingBean.getPassword();
        
        try {
            Customer customer = accountHandler.findCustomerByUsername(username);
            
            if(customer.getPassword().equals(password)) {
                outcome = "index.xhtml";
            }
            else {
                
                throw new Exception("Invalid credentials");
                
            }
        }
        catch(Exception ex) {
            if(ex.getMessage().equalsIgnoreCase("No user registered by that username")) {
                try {
                    Administrator admin = accountHandler.findAdministratorByUsername(username);
                    if(admin.getPassword().equals(password)) {
                        outcome = "/administrator/adminHome.xhtml";//<--HELLO
                        backingBean.setUsername("");
                        backingBean.setPassword("");
                    }
                }
                catch(Exception exp) {
                    backingBean.setErrorMessage("Invalid username or password");
                }
            }
        }
        
        return outcome;
    }
    
}
