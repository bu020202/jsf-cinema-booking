package commands;

import database.active_record.SessionHandler;
import managed_Beans.AdministratorBB;
import managed_Beans.BackingBean;

public class AddSession_AdminCommand implements Command {
    
    private final AdministratorBB backingBean_Admin;
    
    public AddSession_AdminCommand(BackingBean bb) {
        backingBean_Admin = (AdministratorBB)bb;
    }

    @Override
    public String execute() {
        
        String view = "";
        
        SessionHandler sessionHandler = new SessionHandler();
        
        if(sessionHandler.createSession(backingBean_Admin.getSessionToCreate())) {
            backingBean_Admin.setSessionToCreate(null);
            view = "viewEditSessions.xhtml";
        }
        
        return view;
    }
}