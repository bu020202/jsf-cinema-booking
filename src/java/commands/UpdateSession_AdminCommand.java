package commands;

import database.active_record.SessionHandler;
import managed_Beans.AdministratorBB;
import managed_Beans.BackingBean;

public class UpdateSession_AdminCommand implements Command{
    
    private final AdministratorBB backingBean_Admin;
    
    public UpdateSession_AdminCommand(BackingBean bb) {
        backingBean_Admin = (AdministratorBB)bb;
    }

    @Override
    public String execute() {
        
        String view = "Error";
        SessionHandler sessionHandler = new SessionHandler();
        if(sessionHandler.updateSession(backingBean_Admin.getSessionToUpdate())) {
            backingBean_Admin.setSessionToUpdate(null);
            view = "viewEditSessions.xhtml";
        }
        
        return view;
    }
}