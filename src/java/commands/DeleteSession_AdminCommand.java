package commands;

import beans.Session;
import beans.Ticket;
import database.active_record.SessionHandler;
import database.active_record.TicketHandler;
import java.util.ArrayList;
import managed_Beans.AdministratorBB;
import managed_Beans.BackingBean;

public class DeleteSession_AdminCommand implements Command{
    
    private final AdministratorBB backingBean_Admin;
    private final TicketHandler ticketHandler;
    private final SessionHandler sessionHandler;
    
    public DeleteSession_AdminCommand(BackingBean bb) {
        backingBean_Admin = (AdministratorBB)bb;
        ticketHandler = new TicketHandler();
        sessionHandler = new SessionHandler();
   }

    @Override
    public String execute() {
        
        String view = "/error.xhtml";
        
        try {
            Session sessionToDelete = backingBean_Admin.getSessionToDelete();
            ArrayList<Ticket> ticketsToDelete = ticketHandler.findTicketsBySessionId(sessionToDelete.getSessionId());

            if(ticketsToDelete.size() > 0) {
                for(Ticket ticket : ticketsToDelete) {
                    ticketHandler.deleteTicket(ticket);
                }
            }

            sessionHandler.deleteSession(sessionToDelete);
            backingBean_Admin.setSessionToDelete(null);

            view = "viewEditSessions.xhtml";
        }
        catch(Exception ex) {
            
        }
        
        return view;
    }
}