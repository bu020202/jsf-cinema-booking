package commands;

import managed_Beans.*;

public abstract class CommandFactory {
    
    public static Command createCommand(String userCommand, BackingBean bb) {
        
        Command command = null;

        switch(userCommand) {
            case CustomerBB.VALIDATE_CREDENTIALS : 
                command = new ValidateCredentials_Command(bb);break;
            case BackingBean.GET_ALL_FILMS : 
                command = new GetAllFilms_Command(bb);break;
            case AdministratorBB.ADD_FILM :
                command = new AddFilm_AdminCommand(bb);break;
            case AdministratorBB.UPDATE_FILM : 
                command = new UpdateFilm_AdminCommand(bb);break;
            case AdministratorBB.DELETE_FILM :
                command = new DeleteFilm_AdminCommand(bb);break;
            case BackingBean.GET_FILM_SESSIONS :
                command = new GetFilmSessions_Command(bb);break;
            case AdministratorBB.ADD_SESSION :
                command = new AddSession_AdminCommand(bb);break;
            case AdministratorBB.UPDATE_SESSION : 
                command = new UpdateSession_AdminCommand(bb);break;
            case AdministratorBB.DELETE_SESSION :
                command = new DeleteSession_AdminCommand(bb);break;
            case CustomerBB.GET_CUSTOMER_TICKETS :
                command = new GetCustomerTickets_Command(bb);break;
            case CustomerBB.DELETE_TICKET :
                command = new DeleteTicket_Command(bb);break;
            case CustomerBB.ADD_TICKET :
                command = new AddTickets_Command(bb);break;
            default : System.out.println("Invalid command requested");break;
        }       
        return command;
    }
}