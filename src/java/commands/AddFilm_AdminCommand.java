package commands;

import database.active_record.FilmHandler;
import managed_Beans.AdministratorBB;
import managed_Beans.BackingBean;

public class AddFilm_AdminCommand implements Command {
    
    private final AdministratorBB backingBean_Admin;
    
    public AddFilm_AdminCommand(BackingBean bb) {
        backingBean_Admin = (AdministratorBB)bb;
    }
    
    @Override
    public String execute() {
        
        String view = null;
        FilmHandler filmHandler = new FilmHandler();
        if(filmHandler.createFilm(backingBean_Admin.getFilmToCreate())) {
            backingBean_Admin.setFilmToCreate(null);
            view = "adminHome.xhtml";
        }
        else {
            
        }
        return view;
    }
}
