package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class InputValidator implements Validator
{

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException
    {
        
        String componentId = component.getId();
        
        try {
            switch(componentId) {
                case "ticketNumberInput" :
                    validateNumberOfTickets(value);break;
                case "runtime" :
                    validateRuntime(value);break;
                default : throw new Exception("Critical Error: Component ID not reconised");

            }
        }
        catch(Exception e) {
            throw new ValidatorException(new FacesMessage(e.getMessage()));
        }
    }
    
    private void validateNumberOfTickets(Object value) throws Exception{
        
        int numberOfTickets = (Integer)value;
        
        if(numberOfTickets < 1) {
            throw new Exception("Error: You can not book 0 tickets");
        }
    }
    
    private void validateRuntime(Object value) throws Exception {
        
        int runtime = (Integer)value;
        
        if(runtime < 1) {
            throw new Exception("Error: Runtime can not be less than 1 minute.");
        }
    }
}