package managed_Beans;

import beans.Film;
import beans.Session;
import beans.Ticket;
import java.io.Serializable;
import java.util.ArrayList;
import commands.Controller;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class CustomerBB extends BackingBean implements Serializable {
    
    public static final String GET_CUSTOMER_TICKETS = "GET_CUSTOMER_TICKETS";
    public static final String ADD_TICKET = "ADD_TICKET";
    public static final String DELETE_TICKET = "DELETE_TICKET";
    
    private transient Film filmToDisplay = null;
    
    private transient Session sessionToDisplay = null;
    
    private transient Ticket ticketToCreate = null;
    private transient Ticket ticketToDelete = null;
    private transient Ticket ticketToDisplay = null;
    
    private int numberOfTicketsToCreate;
    
    private final transient Controller controller;

    private ArrayList<Ticket> tickets;
    
    public CustomerBB() {
        clearFields();
        controller = new Controller();
    }
    
    private void clearFields() {
        username = "";
        password = "";
        filmToDisplay = null;
        sessionToDisplay = null;
        ticketToCreate = null;
        ticketToDelete = null;
        ticketToDisplay = null;
        numberOfTicketsToCreate = 1;
        films = null;
        sessions = null;
    }
    
    public String validateCredentials() {
        
        String outcome = controller.executeCommand(VALIDATE_CREDENTIALS, this);
        
        if(outcome == null) {
            outcome = "";
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, errorMessage, errorMessage);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
        return outcome;
    }
    
    public int getNumberOfTicketsToCreate() {
        return numberOfTicketsToCreate;
    }
    
    public void setNumberOfTicketsToCreate(int num) {
        numberOfTicketsToCreate = num;
    }
    
    public boolean isLoggedIn() {
        return !(username.equals("") || username == null);
    }
    
    
    @Override
    public Film getFilmToDisplay() {
        return filmToDisplay;
    }
    
    public void setFilmToDisplay(Film filmToDisplay) {
        this.filmToDisplay = filmToDisplay;
    }
    
    public Session getSessionToDisplay() {
        return sessionToDisplay;
    }
    
    public void setSessionToDisplay(Session sessionToDisplay) {
        this.sessionToDisplay = sessionToDisplay;
    }
    
    public Ticket getTicketToDisplay() {
        return ticketToDisplay;
    }
    
    public void setTicketToDisplay(Ticket ticketToDisplay) {
        this.ticketToDisplay = ticketToDisplay;
    }

    public Ticket getTicketToCreate() {
        return ticketToCreate;
    }
    
    public void setTicketToCreate(Ticket ticketToCreate) {
        this.ticketToCreate = ticketToCreate;
    }
    
    public Ticket getTicketToDelete() {
        return ticketToDelete;
    }
    
    public void setTicketToDelete(Ticket ticketToDelete) {
        this.ticketToDelete = ticketToDelete;
    }
    
    public ArrayList<Ticket> getTickets() {
        controller.executeCommand(GET_CUSTOMER_TICKETS, this);
        return tickets;
    }
    
    public void setTickets(ArrayList<Ticket> tickets) {
        this.tickets = tickets;
    }
    
    @Override
    public ArrayList<Film> getFilms() {
        controller.executeCommand(GET_ALL_FILMS, this);
        return films;
    }
    
    @Override
    public void setFilms(ArrayList<Film> films) {
        this.films = films;
    }
    
    public ArrayList<Session> getSessions() {
        controller.executeCommand(GET_FILM_SESSIONS, this);
        return sessions;
    }
    
    @Override
    public void setSessions(ArrayList<Session> sessions) {
        this.sessions = sessions;
    }
    
    public String cancelReservation() {
        return controller.executeCommand(DELETE_TICKET, this);
    }
    
    public String bookTicket() {
        return controller.executeCommand(ADD_TICKET, this);
    }
    
    public String navigateToFilmSessions(Film film) {
        filmToDisplay = film;
        return "filmSessions.xhtml";
    }
    
    public String navigateToLoginLogout() {

        String view;
        if(isLoggedIn()) {
            clearFields();
            view = "logout.xhtml";
        }
        else {
            view = "login.xhtml";
        }
        
        return view;
        
    }
    
    public String navigateToDeleteTicket(Ticket ticket) {
        ticketToDelete = ticket;
        return "deleteTicket.xhtml";
    }
    
    public String navigateToBookTicket(Session session) {
        sessionToDisplay = session;
        ticketToCreate = new Ticket();
        ticketToCreate.setCustomerUsername(username);
        ticketToCreate.setSessionId(session.getSessionId());
        return "bookTicket.xhtml";
    }
}