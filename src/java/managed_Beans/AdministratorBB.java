package managed_Beans;

import beans.Film;
import beans.Session;
import java.io.Serializable;
import java.util.ArrayList;
import commands.Controller;

public class AdministratorBB extends BackingBean implements Serializable{

    public final static String ADD_FILM = "ADMIN_ADD_FILM";
    public final static String UPDATE_FILM = "ADMIN_UPDATE_FILM";
    public final static String DELETE_FILM = "ADMIN_DELETE_FILM";
    
    public final static String ADD_SESSION = "ADMIN_ADD_SESSION";
    public final static String UPDATE_SESSION = "ADMIN_UPDATE_SESSION";
    public final static String DELETE_SESSION = "ADMIN_DELETE_SESSION";
    
    private transient Film filmToCreate = null;
    private transient Film filmToDelete = null;
    private transient Film filmToUpdate = null;
    private transient Film filmToDisplay = null;
    
    private transient Session sessionToCreate = null;
    private transient Session sessionToDelete = null;
    private transient Session sessionToUpdate = null;
    private transient Session sessionToDisplay = null;
     
    private final transient Controller controller;
    
    public AdministratorBB() {
        clearFields();
        controller = new Controller();
    }
    
    private void clearFields() {
   
        username = "";
        password = "";        
        filmToCreate = null;
        filmToDelete = null;
        filmToUpdate = null;
        filmToDisplay = null;
        sessionToCreate = null;
        sessionToDelete = null;
        sessionToUpdate = null;
        sessionToDisplay = null;
        films = null;
        sessions = null;
        
    }
    
    public Film getFilmToCreate() {
        return filmToCreate;
    }

    public void setFilmToCreate(Film filmToCreate) {
        this.filmToCreate = filmToCreate;
    }

    public Film getFilmToDelete() {
        return filmToDelete;
    }

    public void setFilmToDelete(Film filmToDelete) {
        this.filmToDelete = filmToDelete;
    }

    public Film getFilmToUpdate() {
        return filmToUpdate;
    }

    public void setFilmToUpdate(Film filmToUpdate) {
        this.filmToUpdate = filmToUpdate;
    }

    @Override
    public Film getFilmToDisplay() {
        return filmToDisplay;
    }

    public void setFilmToDisplay(Film filmToDisplay) {
        this.filmToDisplay = filmToDisplay;
    }

    public Session getSessionToCreate() {
        return sessionToCreate;
    }
    
    public void setSessionToCreate(Session sessionToCreate) {
        this.sessionToCreate = sessionToCreate;
    }
    
    public Session getSessionToDelete() {
        return sessionToDelete;
    }
    
    public void setSessionToDelete(Session sessionToDelete) {
        this.sessionToDelete = sessionToDelete;
    }
    
    public Session getSessionToUpdate() {
        return sessionToUpdate;
    }
    
    public void setSessionToUpdate(Session sessionToUpdate) {
        this.sessionToUpdate = sessionToUpdate;
    }
    
    public Session getSessionToDisplay() {
        return sessionToDisplay;
    }
    
    public void setSessionToDisplay(Session sessionToDisplay) {
        this.sessionToDisplay = sessionToDisplay;
    }
    
    @Override
    public ArrayList<Film> getFilms() {
        controller.executeCommand(GET_ALL_FILMS, this);     
        return films;
    }
    
    @Override
    public void setFilms(ArrayList<Film> films) {
        this.films = films;
    }
    
    public ArrayList<Session> getSessions() {
        controller.executeCommand(GET_FILM_SESSIONS, this);
        return sessions;
    }
    
    @Override
    public void setSessions(ArrayList<Session> sessions) {
        this.sessions = sessions;
    }
    
    public String addFilm() {
        return controller.executeCommand(ADD_FILM, this);
    }
    
    public String modifyFilmDetails() {
        return controller.executeCommand(UPDATE_FILM, this);
    }
    
    public String removeFilm() {
        return controller.executeCommand(DELETE_FILM, this);
    }
    
    public String addSession() {
        return controller.executeCommand(ADD_SESSION, this);
    }
    
    public String modifySessionDetails() {
        return controller.executeCommand(UPDATE_SESSION, this);
    }
    
    public String removeSession() {
        return controller.executeCommand(DELETE_SESSION, this);
    }
    
    public String navigateToCreateFilm() {
        filmToCreate = new Film();
        return "addFilm.xhtml";
    }
    
    public String navigateToUpdateFilm(Film film) {
        filmToUpdate = film;
        return "updateFilmDetails.xhtml";
    }
    
    public String navigateToDeleteFilm(Film film) {
        filmToDelete = film;
        return "deleteFilm.xhtml";
    }
    
    public String navigateToFilmSessions(Film film) {
        filmToDisplay = film;
        return "viewEditSessions.xhtml";
    }
    
    public String navigateToCreateSession() {
        sessionToCreate = new Session();
        sessionToCreate.setFilmId(filmToDisplay.getFilmId());
        return "addSession.xhtml";
    }
    
    public String navigateToUpdateSession(Session session) {
        sessionToUpdate = session;
        return "updateSessionDetails.xhtml";
    }
    
    public String navigateToDeleteSession(Session session) {
        sessionToDelete = session;
        return "deleteSession.xhtml";
    }
    
    public String navigateToViewSession(Session session) {
        sessionToDisplay = session;
        return "viewSession.xhtml";
    }
    
    public String logout() {
        clearFields();
        return "/logout.xhtml";
    }
}