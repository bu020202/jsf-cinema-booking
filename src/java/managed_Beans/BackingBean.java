package managed_Beans;

import beans.Film;
import beans.Session;
import java.util.ArrayList;

public abstract class BackingBean {
    
    public final static String GET_ALL_FILMS = "GET_ALL_FILMS";
    public final static String GET_FILM_SESSIONS = "GET_FILM_SESSIONS";
    public final static String VALIDATE_CREDENTIALS = "VALIDATE_CREDENTIALS";
    
    String errorMessage;
    String username = null;
    String password = null;
    
    ArrayList<Film> films;
    ArrayList<Session> sessions;
    
      
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }
    
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    public abstract ArrayList<Film> getFilms();
    
    public abstract void setFilms(ArrayList<Film> films);
    
    public abstract void setSessions(ArrayList<Session> session);
    
    public abstract Film getFilmToDisplay();    
}