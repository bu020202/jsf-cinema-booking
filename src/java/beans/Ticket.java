package beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

public class Ticket implements Serializable{
    
    private int ticketId;
    private String customerUsername;
    private int sessionId;
    private String filmTitle;
    private String cinemaName;
    private String screenName;
    private Date dateOfSession;
    private Time sessionStartTime;
    
    public Ticket() {
        
        ticketId = 0;
        customerUsername = "";
        sessionId = 0;
        filmTitle = "";
        cinemaName = "";
        screenName = "";
        dateOfSession = null;
        sessionStartTime = null;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public String getCustomerUsername() {
        return customerUsername;
    }

    public void setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }
    
    public String getFilmTitle() {
        return filmTitle;
    }
    
    public void setFilmTitle(String filmTitle) {
        this.filmTitle = filmTitle;
    }
    
    public String getCinemaName() {
        return cinemaName;
    }
    
    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }
    
    public String getScreenName() {
        return screenName;
    }
    
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
    
    public Date getDateOfSession() {
        return dateOfSession;
    }
    
    public void setDateOfSession(Date dateOfSession) {
        this.dateOfSession = dateOfSession;
    }
    
    public Time getSessionStartTime() {
        return sessionStartTime;
    }
    
    public void setSessionStartTime(Time sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }
    
    @Override
    public String toString() {
        return "";
    }
}