package beans;

import java.io.Serializable;

public class User implements Serializable{
    
    protected String username;
    protected String password;
    
    public User() {
        username = "";
        password = "";
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    @Override
    public String toString() {
        return "";
    }  
}