package beans;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

public class Session implements Serializable{
    
    private int sessionId;
    private int filmId;
    private int screenId;
    private String screenName;
    private String cinemaName;
    private Date date;
    private Time time;
    
    public Session() {
        
        sessionId = 0;
        filmId = 0;
        screenId = 0;
        date = null;
        time = null;
        screenName = "";
        cinemaName = "";
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public int getScreenId() {
        return screenId;
    }

    public void setScreenId(int screenId) {
        this.screenId = screenId;
    }
    
    public String getScreenName() {
        return screenName;
    }
    
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
    
    public String getCinemaName() {
        return cinemaName;
    }
    
    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }
    
    @Override
    public String toString() {
        return "";
    }
}