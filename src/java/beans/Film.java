package beans;

import java.io.Serializable;
import java.sql.Date;

public class Film implements Serializable {

    private int filmId;
    private String title;
    private String description;
    private String classification;
    private int runtime;
    private Date releaseDate;
    
    public Film() {
        
        filmId = 0;
        title = "";
        description = "";
        classification = "";
        runtime = 0;
        releaseDate = null;
    }
    
    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getClassification() {
        return classification;
    }
    
    public void setClassification(String classification) {
        this.classification = classification;
    }
    
    public int getRuntime() {
        return runtime;
    }
    
    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
    
    @Override
    public String toString() {
        return "FilmID:"+filmId+" Title:"+title+" Description:"+description;
    }
}