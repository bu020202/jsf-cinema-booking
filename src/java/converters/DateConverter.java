package converters;

import java.sql.Date;
import java.text.SimpleDateFormat;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("dateConverter")
public class DateConverter implements Converter{
    
    private final static SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy");

     public DateConverter() {
        SDF.setLenient(false);
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        
        if (value.isEmpty()) {
            return value;
        }
        
        try {
            return new Date(SDF.parse(value).getTime());
        }
        catch (Exception e) {
            
            String msgDetail = "Error: '" + value + "' is not a valid date in the form <dd-MM-yyyy>";
            FacesMessage msg = new FacesMessage("Invalid date format", msgDetail);
            throw new ConverterException(msg, e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        
        if (value == null) {
            return "";
        }

        if (value instanceof Date) {
            return SDF.format((Date)value);
        }

        String msgDetail = "Unexpected type " + value.getClass().getName();
        FacesMessage msg = new FacesMessage("Date conversion error", msgDetail);
        //FacesContext.getCurrentInstance().addMessage(component.getClientId(), msg);
        throw new ConverterException(msg);
    } 
}