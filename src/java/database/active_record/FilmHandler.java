package database.active_record;
import beans.Film;
import database.connection.ConnectionManager;
import database.connection.DatabaseConnection;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FilmHandler {
    
    private final String FIND_ALL_FILMS = "SELECT * FROM film";
    private final String FIND_FILM_BY_ID = "SELECT * FROM film WHERE film_id=?";
    
    
    public FilmHandler() {
        
    }
    
    public boolean updateFilm(Film film) {
        
        boolean filmUpdated = false;
        
        Film_ActiveRecord ar = new Film_ActiveRecord(film);
        
        try {
            ar.update();
            filmUpdated = true;
        }
        catch(Exception ex) {
            
        }
        
        return filmUpdated;
    }
    
    public boolean createFilm(Film film) {
        
        boolean filmCreated = false;
        
        Film_ActiveRecord ar = new Film_ActiveRecord(film);
        
        try {
            ar.insert();
            filmCreated = true;
        }
        catch(Exception ex) {
            
        }
        
        return filmCreated;
    }
    
    public boolean deleteFilm(Film film) throws Exception{
        
        boolean filmDeleted = false;
        
        Film_ActiveRecord ar = new Film_ActiveRecord(film);
        
        try {
            ar.delete();
            filmDeleted = true;
        }
        catch (Exception ex) {
            throw ex;
        }
        
        return filmDeleted;
    }
    
    public ArrayList<Film> findAllFilms() {
        
        ArrayList<Film> films = null;
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(FIND_ALL_FILMS);
        
        try {
            con.executePreparedStatement();
            films = createFilmObjects(con.getResultSet());
        }
        catch(Exception ex) {
            
        }
        finally {
            con.close();
        }
        
        return films;
    }
    
    public Film findFilmById(int filmId) {
        
        Film film = null;
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(FIND_FILM_BY_ID);
        con.setStatementParameter(1, filmId);
        
        try {
            con.executePreparedStatement();
            ArrayList<Film> result = createFilmObjects(con.getResultSet());
                
            if(result.size() == 1) {
                film = result.get(0);               
            }
            else {
                throw new Exception("Film is not listed in the database");
            }
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        finally {
            con.close();
        }

        return film;
    }
    
    public ArrayList<Film> createFilmObjects(ResultSet rs) {
        
        ArrayList<Film> films = new ArrayList<>();
        
        try {
            
            while(rs.next()) {
                
                Film film = new Film();
                film.setFilmId(rs.getInt("Film_Id"));
                film.setTitle(rs.getString("Title"));
                film.setDescription(rs.getString("Description"));
                film.setClassification(rs.getString("Classification"));
                film.setRuntime(rs.getInt("Runtime"));
                film.setReleaseDate(rs.getDate("Release_Date"));
                
                films.add(film);
            }
            
        }
        catch(SQLException sqlex) {
            
        }
        
        return films;
    }
}