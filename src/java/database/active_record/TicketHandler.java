package database.active_record;

import beans.Ticket;
import database.connection.ConnectionManager;
import database.connection.DatabaseConnection;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketHandler {
    
//    private final String FIND_TICKETS_BY_USERNAME = "SELECT * FROM ticket WHERE customer_username=?";
//    private final String FIND_TICKETS_BY_SESSION_ID = "SELECT * FROM TICKET WHERE SESSION_ID=?";
    
    private final String FIND_TICKETS_BY_USERNAME = "SELECT * FROM TICKETVIEW WHERE customer_username=?";
    private final String FIND_TICKETS_BY_SESSION_ID = "SELECT * FROM TICKETVIEW WHERE SESSION_ID=?";
    
    public boolean updateTicket(Ticket ticket) {
        
        boolean ticketUpdated = false;
        
        Ticket_activeRecord ar = new Ticket_activeRecord(ticket);
        
        try {
            ar.update();
            ticketUpdated = true;
        }
        catch(Exception ex) {
            
        }
        return ticketUpdated;
    }
    
    public boolean createTicket(Ticket ticket) {
        
        boolean ticketCreated = false;
        
        Ticket_activeRecord ar = new Ticket_activeRecord(ticket);
        
        try {
            ar.insert();
            ticketCreated = true;
        }
        catch(Exception ex) {
            
        }
        return ticketCreated;
    }
    
    public boolean deleteTicket(Ticket ticket) {
        
        boolean ticketDeleted = false;
        
        Ticket_activeRecord ar = new Ticket_activeRecord(ticket);
        
        try {
            ar.delete();
            ticketDeleted = true;
        }
        catch(Exception ex) {
            
        }
        return ticketDeleted;
    }
    
    public ArrayList<Ticket> findTicketsByUsername(String username) {
        
        ArrayList<Ticket> tickets = null;
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(FIND_TICKETS_BY_USERNAME);
        con.setStatementParameter(1, username);
        
        try {
            con.executePreparedStatement();
            tickets = createTicketObjects(con.getResultSet());
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        finally {
            con.close();
        }
        
        return tickets;
    }
    
    public ArrayList<Ticket> findTicketsBySessionId(int sessionId) {
        
        ArrayList<Ticket> tickets = null;
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(FIND_TICKETS_BY_SESSION_ID);
        con.setStatementParameter(1, sessionId);
        
        try {
            con.executePreparedStatement();
            tickets = createTicketObjects(con.getResultSet());
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        finally {
            con.close();
        }
        
        return tickets;
    }
    
    private ArrayList<Ticket> createTicketObjects(ResultSet rs) {
        
        ArrayList<Ticket> tickets = new ArrayList<>();
        
        try {
            
            while(rs.next()) {
                Ticket ticket = new Ticket();
                ticket.setTicketId(rs.getInt("Ticket_ID"));
                ticket.setSessionId(rs.getInt("Session_ID"));
                ticket.setCustomerUsername(rs.getString("Customer_Username"));
                
                ticket.setFilmTitle(rs.getString("Title"));
                ticket.setCinemaName(rs.getString("Cinema"));
                ticket.setScreenName(rs.getString("Screen_Name"));
                ticket.setDateOfSession(rs.getDate("Date"));
                ticket.setSessionStartTime(rs.getTime("Time"));
                
                tickets.add(ticket);
            }
        }
        catch(SQLException sqle) {
            
        }
        return tickets;
    }
}
