package database.active_record;

import beans.Session;
import database.connection.ConnectionManager;
import database.connection.DatabaseConnection;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SessionHandler {
    
    //private final String FIND_SESSIONS_BY_FILM_ID = "SELECT * FROM session WHERE film_id=?";
    private final String FIND_SESSIONS_BY_FILM_ID = "SELECT * FROM SESSIONVIEW WHERE film_id=?";
    
    public boolean updateSession(Session session) {
        
        boolean sessionUpdated = false;
        
        Session_activeRecord ar = new Session_activeRecord(session);
        
        try {
            ar.update();
            sessionUpdated = true;
        }
        catch(Exception ex) {
            
        }
        
        return sessionUpdated;
    }
    
    public boolean createSession(Session session) {
        
        boolean sessionCreated = false;
        
        Session_activeRecord ar = new Session_activeRecord(session);
        
        try{
            ar.insert();
            sessionCreated = true;
        }
        catch (Exception ex) {
            
        }
        
        return sessionCreated;
    }
    
    public boolean deleteSession(Session session) {
        
        boolean sessionDeleted = false;
        
        Session_activeRecord ar = new Session_activeRecord(session);
        
        try {
            ar.delete();
            sessionDeleted = true;
        }
        catch(Exception ex) {
            
        }
        
        return sessionDeleted;
    }
    
    public ArrayList<Session> findSessionsByFilmId(int filmId) {
        
        ArrayList<Session> filmSessions = null;
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(FIND_SESSIONS_BY_FILM_ID);
        con.setStatementParameter(1, filmId);
        
        try {
            con.executePreparedStatement();
            filmSessions = createSessionObjects(con.getResultSet());
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
        finally {
            con.close();
        }
        
        return filmSessions;
    }
    
    private ArrayList<Session> createSessionObjects(ResultSet rs) {
        
        ArrayList<Session> sessions = new ArrayList<>();
        
        try {
            while(rs.next()) {
                Session session = new Session();
                session.setSessionId(rs.getInt("Session_ID"));
                session.setFilmId(rs.getInt("Film_ID"));
                session.setScreenId(rs.getInt("Screen_ID"));
                session.setDate(rs.getDate("Date"));
                session.setTime(rs.getTime("Time"));
                
                session.setScreenName(rs.getString("Screen_Name"));
                session.setCinemaName(rs.getString("NAME"));
                
                sessions.add(session);
            }
        }
        catch(SQLException sqlex) {
            
        }
        
        return sessions;
    }
}
