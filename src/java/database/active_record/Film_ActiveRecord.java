package database.active_record;

import beans.Film;
import database.connection.ConnectionManager;
import database.connection.DatabaseConnection;

public class Film_ActiveRecord implements ActiveRecord {
    
    private final String INSERT_FILM = "INSERT INTO film (TITLE, DESCRIPTION, CLASSIFICATION, RUNTIME, RELEASE_DATE) VALUES (?, ?, ?, ?, ?)";
    private final String UPDATE_FILM = "UPDATE film SET title=?, description=?, classification=?, runtime=?, release_date=? WHERE film_Id=?";
    private final String DELETE_FILM = "DELETE FROM film WHERE film_Id=?";
    
    private final Film film;
    
    public Film_ActiveRecord(Film film) {
        this.film = film;
    }
    
    @Override
    public void insert() throws Exception {
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(INSERT_FILM);
        con.setStatementParameter(1, film.getTitle());
        con.setStatementParameter(2, film.getDescription());
        con.setStatementParameter(3, film.getClassification());
        con.setStatementParameter(4, film.getRuntime());
        con.setStatementParameter(5, film.getReleaseDate());
        
        con.executePreparedStatement();
        int updateCount = con.getUpdateCount();
        con.close();
        
        if(updateCount < 1) throw new Exception("Could not insert film into database, FilmID: " + film.getFilmId());
        
    }

    @Override
    public void update() throws Exception {
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(UPDATE_FILM);
        con.setStatementParameter(1, film.getTitle());
        con.setStatementParameter(2, film.getDescription());
        con.setStatementParameter(3, film.getClassification());
        con.setStatementParameter(4, film.getRuntime());
        con.setStatementParameter(5, film.getReleaseDate());
        con.setStatementParameter(6, film.getFilmId());
        
        con.executePreparedStatement();
        int updateCount = con.getUpdateCount();
        con.close();
        
        if(updateCount < 1) throw new Exception("Could not update film details, FilmID: " + film.getFilmId());
    }

    @Override
    public void delete() throws Exception {
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(DELETE_FILM);
        con.setStatementParameter(1, film.getFilmId());
        
        try {
           con.executePreparedStatement(); 
           int updateCount = con.getUpdateCount();
           if(updateCount < 1) throw new Exception("Could not remove film from database, FilmID: " + film.getFilmId());
        }
        catch(Exception ex) {
            throw ex;
        }
        finally {
            con.close();
        }
    } 
}