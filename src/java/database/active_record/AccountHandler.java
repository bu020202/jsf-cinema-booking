package database.active_record;

import beans.Administrator;
import beans.User;
import beans.Customer;
import database.connection.ConnectionManager;
import database.connection.DatabaseConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AccountHandler {
    
    private final String FIND_CUSTOMER_BY_USERNAME = "SELECT * FROM customer WHERE username=?";
    private final String FIND_ADMINISTRATOR_BY_USERNAME = "SELECT * FROM administrator WHERE username=?";
    
    public AccountHandler() {
        
    }
    
    public Customer findCustomerByUsername (String username) throws Exception {
        Customer customer = null;
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(FIND_CUSTOMER_BY_USERNAME);
        con.setStatementParameter(1, username);
        
        try {
            con.executePreparedStatement();
            ArrayList<User> result = createUserObjects(con.getResultSet());
            
            if(result.size() == 1) {
                customer = new Customer();
                customer.setUsername(result.get(0).getUsername());
                customer.setPassword(result.get(0).getPassword());
            }
            else {
                throw new Exception("No user registered by that username");
            }
        }
        catch(Exception ex) {
            throw ex;
        }
        finally {
            con.close();
        }
        
        return customer;
    }
    
    public Administrator findAdministratorByUsername (String username) throws Exception {
        
        Administrator admin = null;
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(FIND_ADMINISTRATOR_BY_USERNAME);
        con.setStatementParameter(1, username);
        
        try {
            con.executePreparedStatement();
            ArrayList<User> result = createUserObjects(con.getResultSet());
            
            if(result.size() == 1) {
                admin = new Administrator();
                admin.setUsername(result.get(0).getUsername());
                admin.setPassword(result.get(0).getPassword());
            }
            else {
                throw new Exception("No user registered by that username");
            }
        }
        catch(Exception ex) {
            throw ex;
        }
        finally {
            con.close();
        }
        
        return admin;
    }
    
    public ArrayList<User> createUserObjects(ResultSet rs) {
        
        ArrayList<User> users = new ArrayList<>();
        
        try {
            
            while(rs.next()) {
                User user = new User();
                user.setUsername(rs.getString("Username"));
                user.setPassword(rs.getString("Password"));
                
                users.add(user);
            }
        }
        catch(SQLException sqlex) {
            
        }
        return users;
    }
}
