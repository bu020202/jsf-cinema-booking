package database.active_record;

import beans.Ticket;
import database.connection.ConnectionManager;
import database.connection.DatabaseConnection;

public class Ticket_activeRecord implements ActiveRecord {
    
    private final String INSERT_TICKET = "INSERT INTO TICKET (CUSTOMER_USERNAME, SESSION_ID) VALUES (?, ?)";
    private final String UPDATE_TICKET = "";
    private final String DELETE_TICKET = "DELETE FROM TICKET WHERE TICKET_ID=?";

    private final Ticket ticket;
    
    public Ticket_activeRecord(Ticket ticket) {
        this.ticket = ticket;
    }
    
    @Override
    public void insert() throws Exception {
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(INSERT_TICKET);
        con.setStatementParameter(1, ticket.getCustomerUsername());
        con.setStatementParameter(2, ticket.getSessionId());
        
        con.executePreparedStatement();
        int updateCount = con.getUpdateCount();
        con.close();
        
        if(updateCount < 1) {
            throw new Exception("Could not insert record into the database.");
        }
    }

    @Override
    public void update() throws Exception {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public void delete() throws Exception {
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(DELETE_TICKET);
        con.setStatementParameter(1, ticket.getTicketId());
        
        try {
            con.executePreparedStatement();
            int updateCount = con.getUpdateCount();
            
            if(updateCount < 1) {
                throw new Exception("Could not remove Ticket from database.");
            }
        }
        catch(Exception ex) {
            System.out.println(ex.getMessage());
            throw ex;
        }
        finally {
            con.close();
        }
    }  
}
