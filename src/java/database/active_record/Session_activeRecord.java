package database.active_record;

import beans.Session;
import database.connection.ConnectionManager;
import database.connection.DatabaseConnection;

public class Session_activeRecord implements ActiveRecord{

    private final String INSERT_SESSION = "INSERT INTO session (FILM_ID, SCREEN_ID, DATE, TIME) VALUES (?, ?, ?, ?)";
    private final String UPDATE_SESSION = "UPDATE session SET screen_id=?, date=?, time=? WHERE session_id=?";
    private final String DELETE_SESSION = "DELETE FROM session WHERE session_id=?";
    
    private final Session session;
    
    public Session_activeRecord(Session session) {
        this.session = session;
    }
            
    @Override
    public void insert() throws Exception {
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(INSERT_SESSION);
        con.setStatementParameter(1, session.getFilmId());
        con.setStatementParameter(2, session.getScreenId());
        con.setStatementParameter(3, session.getDate());
        con.setStatementParameter(4, session.getTime());
        
        con.executePreparedStatement();
        int updateCount = con.getUpdateCount();
        con.close();
        
        if(updateCount < 1) {
            throw new Exception("Could not insert record into the database");
        }
    }

    @Override
    public void update() throws Exception {
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(UPDATE_SESSION);
        con.setStatementParameter(1, session.getScreenId());
        con.setStatementParameter(2, session.getDate());
        con.setStatementParameter(3, session.getTime());
        con.setStatementParameter(4, session.getSessionId());
        
        con.executePreparedStatement();
        int updateCount = con.getUpdateCount();
        con.close();
        
        if(updateCount < 1) {
            throw new Exception("Could not update record.");
        }
    }

    @Override
    public void delete() throws Exception {
        
        DatabaseConnection con = ConnectionManager.getInstance().getConnection();
        con.prepareStatement(DELETE_SESSION);
        con.setStatementParameter(1, session.getSessionId());
        
        con.executePreparedStatement();
        int updateCount = con.getUpdateCount();
        con.close();
        //
    }
    
}
